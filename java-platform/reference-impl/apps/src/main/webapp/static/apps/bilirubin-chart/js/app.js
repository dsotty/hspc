'use strict';

angular.module('himssApp', ['highcharts-ng', 'himssApp.filters', 'himssApp.services', 'himssApp.controllers']);