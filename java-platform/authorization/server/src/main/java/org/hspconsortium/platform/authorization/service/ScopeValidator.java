/*
 * #%L
 * hsp-authorization-server
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package org.hspconsortium.platform.authorization.service;

import org.mitre.oauth2.token.StructuredScopeAwareOAuth2RequestValidator;
import org.springframework.security.oauth2.common.exceptions.InvalidScopeException;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.ClientDetails;

public class ScopeValidator extends StructuredScopeAwareOAuth2RequestValidator {

	@Override
	public void validateScope(AuthorizationRequest authorizationRequest, ClientDetails client) throws InvalidScopeException {
		super.validateScope(authorizationRequest, client);
		if (authorizationRequest.getExtensions().get("invalid_launch") != null) {
	 		throw new InvalidScopeException((String)authorizationRequest.getExtensions().get("invalid_launch"));
		}
	}

}
