/*
 * #%L
 * hsp-authorization-server
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package org.hspconsortium.platform.authorization.service;

import org.mitre.oauth2.service.ClientDetailsEntityService;
import org.mitre.openid.connect.request.ConnectOAuth2RequestFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("hspOAuth2RequestFactory")
public class HSPOAuth2RequestFactory extends ConnectOAuth2RequestFactory {

    private String fhirService;

    @Autowired
    public HSPOAuth2RequestFactory(
            ClientDetailsEntityService clientDetailsService) {
        super(clientDetailsService);
    }

    @Autowired
    public void setFhirService(String fhirService) {
        this.fhirService = fhirService;
    }

    @Override
    public AuthorizationRequest createAuthorizationRequest(Map<String, String> inputParams) {
        AuthorizationRequest ret = super.createAuthorizationRequest(inputParams);

        String aud = ret.getRequestParameters().get("aud");
        if (!fhirService.equals(aud)) {
            ret.getExtensions().put("invalid_launch", "Incorrect service URL (aud): " + aud);
        }

        return ret;
    }
}
