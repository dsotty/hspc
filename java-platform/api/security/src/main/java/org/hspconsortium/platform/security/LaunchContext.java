/*
 * #%L
 * hsp-security
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package org.hspconsortium.platform.security;

public final class LaunchContext {

    private Long id;

    private String launchId;

    private String patientId;

    public LaunchContext() {
    }

    public LaunchContext(String launchId, String patientId) {
        this.launchId = launchId;
        this.patientId = patientId;
    }

    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the launchId
     */
    public String getLaunchId() {
        return launchId;
    }

    /**
     * @param launchId the launchId to set
     */
    public void setLaunchId(String launchId) {
        this.launchId = launchId;
    }

    /**
     * @return the patientId
     */
    public String getPatientId() {
        return patientId;
    }

    /**
     * @param patientId the patientId to set
     */
    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

}
